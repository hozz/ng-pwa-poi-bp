import register from './plugin-sw-precache/register-service-worker'
import * as angular from 'angular'
import 'normalize.css/normalize.css'
import 'angular-material/angular-material.scss';
import 'angular-loading-bar/build/loading-bar.css';
import 'angular-loading-bar'
import {myAppName} from './src/app';
import './src/app.router';
import './src/app.router-guard';
import './src/components';
import './src/services';
import './src/filters';
import './src/directives';
import './index.sass';
import 'angular-ui-notification/dist/angular-ui-notification.css';
import 'add-to-homescreen/addtohomescreen'
import 'add-to-homescreen/dist/style/addtohomescreen.css';

setTimeout(() => {
    (window as any).addToHomescreen()
}, 10);

(window as any).angular = angular;
export {myAppName};


register();
