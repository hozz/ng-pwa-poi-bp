import {myAppName} from "../";
import {Trace, Transition, TransitionService} from '@uirouter/angularjs';

myAppName.run(['$trace', '$transitions', '$rootScope',
    function ($trace: Trace, $transitions: TransitionService, $rootScope) {
        $trace.enable('TRANSITION');
        $transitions.onSuccess({to: 'home.*'}, function (trans: Transition) {
        });
        $transitions.onStart({to: 'home.*'}, function (trans: Transition) {
            // const {go} = trans.router.stateService;
            // const token = getAppToken();
            // if (!token) {
            //     go('login');
            // }
        });

        $transitions.onStart({to: 'login'}, function (trans: Transition) {
            // const token = getAppToken();
            // const {go} = trans.router.stateService;
            // if (token) {
            //     go('home');
            // }
        });

    }]);