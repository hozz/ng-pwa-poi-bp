import {myAppName} from "../app";

myAppName.filter('logger', function () {
    return (...args) => console.log(...args);
});