import {myAppName} from "../app";
import {IScope} from "angular";

myAppName.directive('disableUntilResolved', function () {
    return {
        link: function (scope: IScope & { disableUntilResolved: (...args: any[]) => Promise<any> }, element, attrs) {
            element.bind('click', () => {
                element.attr('disabled', 1);
                const reEnable = data => {
                    element.removeAttr('disabled');
                    return data;
                };
                scope.disableUntilResolved()
                    .then(reEnable)
                    .catch(reEnable)
                ;
            })
        },
        scope: {
            disableUntilResolved: '&',
        }
    }
});
