import {myAppName} from "./app";
import {StateProvider} from "@uirouter/angularjs";

myAppName.config(['$stateProvider', '$urlRouterProvider', '$locationProvider', function ($stateProvider: StateProvider, $urlRouterProvider, $locationProvider) {
    $stateProvider
        .state({
            name: 'home',
            url: '/home',
            component: 'homeScreen',
        });
    $urlRouterProvider.otherwise("/home/cards");
    $locationProvider.hashPrefix('');

}]);