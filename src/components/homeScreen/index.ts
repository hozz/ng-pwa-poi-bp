import {myAppName} from "../../app";
import controller from './controller';
import './styles.sass';

const template = require('./template.html');


myAppName.component('homeScreen', {
    template,
    controller,
});