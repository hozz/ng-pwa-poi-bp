declare module "*.html" {
    const html: string;
    export default html;
}

declare module '*.json' {
    const json: string;
    export default json;
}