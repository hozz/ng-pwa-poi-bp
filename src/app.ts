import uiRouter from '@uirouter/angularjs'
import * as angularMaterial from 'angular-material';
import 'angular-ui-notification';

export const myAppName = (window as any).angular.module('myAppName', [
    uiRouter,
    angularMaterial,
    'angular-loading-bar',
    'ui-notification',
]);

