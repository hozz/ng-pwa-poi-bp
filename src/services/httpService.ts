import {myAppName} from "../app";
import {IHttpRequestConfigHeaders, IHttpService, IRequestConfig} from "angular";

const getHeaders = (headers: IHttpRequestConfigHeaders = {}) => ({
    // token: getToken(),
    'Content-Type': 'application/json',
    ...headers
});

export class HttpService {
    $http: IHttpService;

    constructor($http: IHttpService) {
        this.$http = $http;
    }

    public request = (request: IRequestConfig) => this.$http({
        ...request,
        headers: getHeaders(request.headers),
        url: process.env.END_POINT + request.url,
    });
}

myAppName.service('httpService', ['$http', HttpService]);

