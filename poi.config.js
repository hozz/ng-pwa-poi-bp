const END_POINT = 'https://example.com';
module.exports = {
    sourceMap: process.env.NODE_ENV === 'development',
    entry: './index.ts',
    publicPath: '/',
    plugins: [
        require('@poi/plugin-typescript')({}),
        require('./plugin-sw-precache')(),
        require('poi-plugin-dotenv')({
            env: {
                PUBLIC_URL: '/',
                END_POINT,
            }
        }),
    ],
    define: {
        'process.env.PUBLIC_URL': JSON.stringify('/'),
    },
    html: {
        template: './static/index.html'
    },
    webpack(config) {
        config.module.rules.push({
            test: /\.(html)$/,
            use: {
                loader: 'html-loader',
                options: {
                    attrs: [':md-svg-icon', ':md-svg-src', 'img:src']
                }
            },
        });
        return config;
    }
};
